/*
MIT License

Copyright (c) 2019 Liam Putnam

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


This code is supplimentary to our report, but if you, Dr. Squier would like to 
use it, it is licensed by the MIT License. Have fun!

- Liam Putnam, Nick Jarmusz, and Alan Durick. Dec 2019.
*/


#include <SPI.h>
#include <Wire.h>

#include <Adafruit_SPITFT.h>
#include <Adafruit_SPITFT_Macros.h>
#include <gfxfont.h> // Font support
#include <Adafruit_GFX.h> // Graphics library
#include <Adafruit_ILI9341.h> // Screen library
#include <Adafruit_FT6206.h> // Touchscreen library
#include <Adafruit_AMG88xx.h> // Infrared camera
#include <SparkFun_TB6612.h> // Motor driver

#include <Fonts/FreeMono9pt7b.h>
#include <Fonts/FreeSans9pt7b.h>

#define BLACK ILI9341_BLACK
#define WHITE ILI9341_WHITE
#define GREEN ILI9341_GREEN
#define RED ILI9341_RED
#define ORANGE ILI9341_ORANGE

#define TFT_CS 10
#define TFT_DC 9

#define TRIG_PIN 27
#define ECHO_PIN 29
#define LED1_PIN 35 // blue
#define LED2_PIN 33 // green
#define LED3_PIN 31 // red

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC);
Adafruit_FT6206 ctp = Adafruit_FT6206();
Adafruit_AMG88xx amg;

static uint16_t tempToColor(float tempLow, float tempHigh, float temp);
static void alarm(float, float, float);
static long sonar();
static void clearBehindText(String s, int x, int y, uint16_t fillColor);

Motor motor1 = Motor(48, 49, 45, 1, 50);
Motor motor2 = Motor(40, 41, 44, 1, 50);

float pixels[AMG88xx_PIXEL_ARRAY_SIZE];
float minTemp = 1000;
float maxTemp = 0;  

//width: 240
//height: 320
int scale = 10;
//int scale = 30;
int spacing = 10;


void setup () {
  Serial.begin(115200);
  //Serial.begin(9600);

  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);

  digitalWrite(39, LOW);
  
  amg.begin();
  tft.begin();
  tft.fillScreen(BLACK);

  tft.setRotation(1);
  // Text stuff
  tft.setTextWrap(false);
  
  tft.setFont(&FreeMono9pt7b);
  spacing = 15;
  //tft.setFont(&FreeSans9pt7b);
  //spacing = 17;
}


float dangerTemp = 30;
int alarmState = 2;
bool enableMotors = false;

void loop () {
  delay(400);
  long distance = sonar();
  tft.setTextColor(WHITE);
  
  alarm(alarmState);
  amg.readPixels(pixels);

  if (ctp.touched()) { enableMotors = !enableMotors; }
  double newX = 3;
  double newY = tft.height()-6;
  tft.setCursor(newX, newY);
  clearBehindText("Motors disabled", newX, newY, BLACK);
  
  tft.print("Motors ");
  if (enableMotors) {
    tft.setTextColor(GREEN);
    tft.println("enabled ");
  } else {
    tft.setTextColor(RED);
    tft.println("disabled");
  }
  tft.setTextColor(WHITE);

  tft.setCursor(3, 8*scale+2*spacing);
  String tempExtremes = "Low "+String(minTemp).substring(0,4)+" High "+String(maxTemp).substring(0,4)+"/"+String(dangerTemp).substring(0,4);
  clearBehindText(tempExtremes, 3, 8*scale+spacing*2, BLACK);
  tft.println(tempExtremes);
  if (distance < 40 && distance > 2) {
        //Serial.print(distance);
        //Serial.println(" cm");
        if (maxTemp >= dangerTemp) {
          alarmState = 0;
        } else {
          alarmState = 1;
        }
        } else {
        //Serial.print(distance);
        //Serial.println(" cm");
        alarmState = 2;
  }
    // Resetting stats each frame
  maxTemp = 0;
  minTemp = 1000;

  for (int i = 0; i < 64; i++) {
   int xIndex = i % 8;
   int yIndex = 7-(i / 8);
   //Serial.println(pixels[i]);
   if (pixels[i] < minTemp) {
      minTemp = pixels[i];
   }
   if (pixels[i] > maxTemp) {
      maxTemp = pixels[i];
   }
   tft.fillRect(scale*xIndex, scale*yIndex, scale, scale, tempToColor(20, 33, pixels[i]));
  }
  Serial.println("MinTemp: "+String(minTemp)+", MaxTemp: "+String(maxTemp));
  // Drawing a border
  int shade = 70;
  tft.drawFastVLine(0, 0, 8*scale+1, tft.color565(shade, shade, shade));
  tft.drawFastHLine(0, 0, 8*scale+1, tft.color565(shade, shade, shade));
  tft.drawFastVLine(8*scale+1, 0, 8*scale+1, tft.color565(shade, shade, shade));
  tft.drawFastHLine(0, 8*scale+1, 8*scale+1, tft.color565(shade, shade, shade));
  
}


uint16_t tempToColor(float tempLow, float tempHigh, float temp) {
  temp = max(tempLow, temp);
  temp = min(tempHigh, temp);
  //Serial.println();
  //Serial.println(temp);
  //Serial.println(tempLow);
  //Serial.println(tempHigh);
  float tempScaled = (temp-tempLow)/(tempHigh-tempLow);
  int r = 0;
  int g = 0;
  int b = 0;
  //Serial.println(tempScaled);
  int tempVal = min(int(6*256*(tempScaled/6.0)),255)%256;

  int mode = 1;
  //Serial.println(tempVal);
  if (mode == 1) {
    return tft.color565(tempVal,0, 0);
  } else if (mode == 0) {
    if (tempScaled < 1/6.0) {
      // black->blue
      b = tempVal;
    } else if (tempScaled < 2/6.0) {
      //blue->cyan
      g = tempVal;
      b = 255;
    } else if (tempScaled < 3/6.0) {
      //cyan->green
      g = 255;
      b = 255-tempVal;
    } else if (tempScaled < 4/6.0) {
      //green->yellow
      r = tempVal;
      g = 255;
    } else if (tempScaled < 5/6.0) {
      //yellow->red
      r = 255;
      g = 255-tempVal;
    } else {
      //red->white   
      r = 255;
      g = tempVal;
      b = tempVal;
    }
  }
  return tft.color565(r, g, b);
}

void alarm(unsigned int flag) 
{
    float newY = tft.height()-7-spacing*1;
    tft.setCursor(3, newY);
    clearBehindText("1234567890", 0, newY, BLACK);
    if(flag == 0)
    {
        // red
        tft.setTextColor(RED);
        tft.println("Danger");
        digitalWrite(LED1_PIN,LOW); // When the Red condition is met, the Green LED should turn off
        digitalWrite(LED2_PIN,LOW);
        digitalWrite(LED3_PIN, HIGH);

    }
    else if(flag == 1)
    {
        // yellow
        tft.setTextColor(ILI9341_YELLOW, ILI9341_BLACK);
        tft.println("Unsafe");
        digitalWrite(LED1_PIN, LOW);
        digitalWrite(LED2_PIN, HIGH);
        digitalWrite(LED3_PIN, HIGH);
    }
    else if(flag == 2)
    {
        // green
        tft.setTextColor(ILI9341_GREEN, ILI9341_BLACK);
        tft.println("Safe");
        digitalWrite(LED1_PIN, LOW);
        digitalWrite(LED2_PIN, HIGH);
        digitalWrite(LED3_PIN, LOW);
      
    }

    if (enableMotors and flag == 0) {
          int power = 40;
          motor1.drive(-power);
          motor2.drive(-power);
        } else {
          motor1.brake();
          motor2.brake();
        }
    tft.setTextColor(ILI9341_WHITE);
}

long sonar() {
    long duration = 0, distance = 0;
    long delayAmount = 10;
    digitalWrite(TRIG_PIN, LOW);
    delayMicroseconds(delayAmount);
    digitalWrite(TRIG_PIN, HIGH); // Sending pulse
    delayMicroseconds(delayAmount);
    digitalWrite(TRIG_PIN, LOW);
    duration = pulseIn(ECHO_PIN, HIGH); // Waiting until 
    distance = (duration / 2) / 29.1;
 
    tft.setCursor(8*scale+spacing/2, spacing);
    String newText = "Echo: " + String(duration/1000.0)+"ms   ";
    clearBehindText(newText, 8*scale+spacing/2, spacing, BLACK);
    tft.println(newText);
    tft.setCursor(8*scale+spacing/2, spacing*2);
    newText = "Distance: "+String(distance)+"cm    ";
    clearBehindText(newText, 8*scale+spacing/2, spacing*2, BLACK);
    tft.println(newText);
    return distance;
}


static void clearBehindText(String s, int x, int y, uint16_t fillColor) {
  int16_t x1, y1;
  uint16_t w, h;
  tft.getTextBounds(s, x, y, &x1, &y1, &w, &h);
  tft.fillRect(x1, y1, w, h, fillColor); 
}
